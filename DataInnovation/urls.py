"""DataInnovation URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.conf import settings
from django.conf.urls.static import static

from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from django.contrib.auth.views import LogoutView
from django.views.generic import RedirectView

from accounts.views import LoginView, RegisterView

from home import views 

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^$', views.home, name='home_url'),
    url(r'^news/$', views.NewsView.as_view(), name='news_url'),
    path('news/<int:pk>/', views.NewsDetailView.as_view()),
    path('news/<int:pk>/delete/', views.NewsDelete.as_view()),
    url(r'^news/create$', views.NewsRegisterView.as_view(), name='create_news'),
    path('news/<int:pk>/edit/', views.NewsDetailUpdateView.as_view(), name='edit_news'),
    path('news/<int:pk>/delete/', views.NewsDelete.as_view(), name='delete_news'),

    path('about/',include('home.urls')),
    
    url(r'^accounts/login/', RedirectView.as_view(url='/login')),
    url(r'^account/', include('accounts.urls', namespace='account')),
    url(r'^account/', include('accounts.passwords.urls')),

    url(r'^login/$', LoginView.as_view(), name='login'),
    url(r'^logout/$', LogoutView.as_view(), name='logout'),
    url(r'^register/$', RegisterView.as_view(), name='register'),

    url(r'^contact/', views.contact, name='contact'),  
    
] 


# if settings.DEBUG:c
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)