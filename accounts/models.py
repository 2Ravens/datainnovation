from datetime import timedelta

from django.conf import settings
from django.db import models
from django.db.models import Q
from django.db.models.signals import pre_save, post_save
from django.contrib.auth.models import(
    AbstractBaseUser, BaseUserManager
)

from django.core.mail import send_mail
from django.template.loader import get_template
from django.urls import reverse
from django.utils import timezone

from DataInnovation.utils import unique_key_generator
 
# send_mail(subject, message, from_email, recipient_list, html_message)

DEFAULT_ACTIVATION_DAYS = getattr(settings, 'DEFAULT_ACTIVATION_DAYS', 7)

class UserManager(BaseUserManager):
    def make_premium(self, user):
        print("We got a supporter here!")
        user_obj = self.get(username=user.username)
        user_obj.premium = True
        user_obj.timestamp = timezone.now()
        user_obj.save()

    def create_user(self, username, password=None, is_active=True, is_staff=False, is_admin=False):
        if not username:
            raise ValueError("Users must have a username")
        if not password:
            raise ValueError("Users must have a password")
        user_obj = self.model(
            # email = self.normalize_email(email),
            username = username,
        )
        user_obj.set_password(password)  # how you would change password too
        user_obj.is_active = is_active
        user_obj.staff = is_staff
        user_obj.admin = is_admin
        user_obj.save(using=self._db)
        return user_obj

    def create_staffuser(self, email=None, username=None, password=None):
        user = self.create_user(
            # email=email,
            username=username,
            password = password,
            is_staff = True
        )
        return user

    def create_superuser(self, email=None, username=None, password=None):
        user = self.create_user(
            username=username,
            password=password,
            is_staff=True,
            is_admin=True,
        )
        return user

class User(AbstractBaseUser):
    email     = models.EmailField(max_length=255, null=False, blank=False)
    first_name = models.CharField(max_length=255, blank=True, null=True)
    last_name = models.CharField(max_length=255, blank=True, null=True)
    username  = models.CharField(max_length=255, blank=False, null=False, unique=True)
    role      = models.CharField(max_length=255, blank=True, null=True)
    summary   = models.TextField(max_length=2000, blank=True, null=True)
    about     = models.TextField(max_length=2000, blank=True, null=True)
    linkedin  = models.URLField(max_length=255, blank=True, null=True)
    website   = models.URLField(max_length=255, blank=True, null=True)
    image     = models.ImageField(upload_to='static/img/',null=True, blank=True)
    active    = models.BooleanField(default=True) #can login
    is_active = models.BooleanField(default=True) #can login
    display   = models.BooleanField(default=False) #can has premium account
    disp_order= models.IntegerField(null=True, blank=True)
    staff     = models.BooleanField(default=False) # staff user, non-superuser
    admin     = models.BooleanField(default=False) # superuser
    timestamp = models.DateTimeField(auto_now=True)
    # confirmed_email = models.BooleanField(default=False)
    # confirmed_date = models.DateTimeField(default=False)

    USERNAME_FIELD = 'username'  # makes email define the user
    # email and password fields are required by defualt.
    REQUIRED_FIELDS = []  #['full_name'] #python manage.py createsuperuser

    objects = UserManager()
    def __str__(self):
        if self.username:
            return self.username
        return self.email
 
    def get_username(self):
        if self.username:
            return self.username
        return self.email

    def get_full_name(self):
        if self.first_name:
            return self.first_name
        return self.email

    def get_short_name(self):
        return self.username
    
    def has_perm(self, perm, obj=None):
        return True
    
    def has_module_perms(self, app_label):
        return True

    @property
    def is_staff(self):
        if self.is_admin:
            return True
        return self.staff

    @property
    def is_admin(self):
        return self.admin

    # @property
    # def is_active(self):
    #     return self.active

class EmailActivationQuerySet(models.query.QuerySet): # EmailActivation.objects.all().confirmable() checks all objects
    def confirmable(self):
        DEFAULT_ACTIVATION_DAYS
        now = timezone.now()
        start_range = now - timedelta(days=DEFAULT_ACTIVATION_DAYS)
        # is the timestamp in this range?
        end_range = now
        return self.filter(
            activated = False,
            forced_expired = False
        ).filter(
            timestamp__gt=start_range,
            timestamp__lte=end_range
        )

class EmailActivationManager(models.Manager):
    def get_queryset(self):
        return EmailActivationQuerySet(self.model, using=self._db)
    
    def confirmable(self):
        return self.get_queryset().confirmable()
    
    def email_exists(self, email):
        return self.get_queryset().filter(Q(email=email)|Q(user__email=email)).filter(activated=False)

class EmailActivation(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    email = models.EmailField()
    key = models.CharField(max_length=120, null=True, blank=True)
    activated = models.BooleanField(default=False)
    forced_expired = models.BooleanField(default=False)
    expired = models.IntegerField(default=7) # Expires in 7 days
    timestamp = models.DateTimeField(auto_now_add=True)
    update = models.DateTimeField(auto_now=True)

    objects = EmailActivationManager()

    def __str__(self):
        return self.email

    def can_activate(self):
        qs = EmailActivation.objects.filter(pk=self.pk).confirmable()
        if qs.exists():
            return True
        return False

    def activate(self):
        if self.can_activate():
            # pre-activation signal here
            user=self.user
            user.is_active = True
            user.save()
            # post-activaiton signal for user
            self.activated = True
            self.save()
            return True
        return False

    def regenrate(self):
        self.key=None
        self.save()
        if self.key is not None:
            return True
        return False

    def send_activation(self):
        if not self.activated and not self.forced_expired:
            if self.key:
                base_url=getattr(settings, 'BASE_URL', 'http://www.2ravens.co.uk/')
                key_path = reverse('account:email-activate', kwargs={'key': self.key}) # use reverse
                path = "{base}{path}".format(base=base_url, path=key_path)
                context = {
                    'path': path,
                    'email': self.email,
                }
                txt_= get_template("registration/emails/verify.txt").render(context)
                html_= get_template("registration/emails/verify.html").render(context)
                subject = '1-click Email Verification'
                from_email = settings.DEFAULT_FROM_EMAIL
                recipient_list = [self.email]
                sent_mail = send_mail(
                    subject, 
                    txt_,
                    from_email,
                    recipient_list,                    
                    html_message=html_,
                    fail_silently=False,
                )
                return sent_mail
        return False

# def pre_save_email_activation(sender, instance, *args, **kwargs):
#     if not instance.activated and not instance.forced_expired: 
#         if not instance.key:
#             instance.key = unique_key_generator(instance)

# pre_save.connect(pre_save_email_activation, sender=EmailActivation)

# def post_save_user_create_receiver(sender, instance, created, *args, **kwargs):
#     if created:
#         obj = EmailActivation.objects.create(user=instance, email=instance.email)
#         obj.send_activation() 

# post_save.connect(post_save_user_create_receiver, sender=User)

class GuestEmail(models.Model):
    email       = models.EmailField()
    active      = models.BooleanField(default=True)
    update      = models.DateTimeField(auto_now=True)
    timestamp   = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.email

