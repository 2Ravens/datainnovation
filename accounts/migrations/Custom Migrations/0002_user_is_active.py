# Bodged together for Django 2.2.6 on 2019-11-21 17:21
# to try and get the fucking 'user.is_active' initiated on heroku
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='is_active',
            field=models.BooleanField(default=True),
        ),
    ]
