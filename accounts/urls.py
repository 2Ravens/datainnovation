from django.conf.urls import url
from django.urls import path

from .views import (
    AccountHomeView,
    AccountEmailActivationView,
    UserDetailUpdateView,
) 

app_name = 'accounts'

urlpatterns = [
    url(r'^$', AccountHomeView.as_view(), name='home'),
    url(r'^details/$', UserDetailUpdateView.as_view(), name='user-update'),
    url(r'^email/confirm/(?P<key>[0-9A-Za-z]+)/$', AccountEmailActivationView.as_view(), name='email-activate'),
    url(r'^email/resend-activation/$', AccountEmailActivationView.as_view(), name='resend-activation'),
]


# account/email/confirm/asdasfd/ -> activaiton view