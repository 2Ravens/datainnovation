from django import forms
from django.contrib.auth import get_user_model, authenticate, login
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.urls import reverse
from django.utils.safestring import mark_safe

from .models import EmailActivation, GuestEmail
from home.models import *
from .signals import user_logged_in

User = get_user_model()

class ReactivateEmailForm(forms.Form):
    email = forms.EmailField()

    def clean_email(self):
        email = self.cleaned_data.get('email')
        qs = EmailActivation.objects.email_exists(email)
        if not qs.exists():
            register_link=reverse('register')
            msg = """This email does not exist, would you like to <a href="{link}">register</a>?""".format(link=register_link)
            raise forms.ValidationError(mark_safe(msg))
        return email

class UserAdminCreationForm(forms.ModelForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ('username',) # 'full-name',)

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(UserAdminCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user

class UserDetailChangeForm(forms.ModelForm):
    username = forms.CharField(label='Name', required=False, widget=forms.TextInput(attrs={"class": 'form-control'}))
    class Meta:
        model = User
        fields = ['username']

class UserAdminChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = User
        fields = ('username', 'password', 'active', 'staff', 'admin')
    
    def clean_password(self):
        '''Regardless of what the user provides, return the intial value.
        This is done here, rather than on the field, because the 
        field does not have access to the initial value'''
        return self.initial["password"]

class GuestForm(forms.ModelForm):
	# email = forms.EmailField(widget=forms.EmailInput(attrs={'class': 'form-control', "placeholder": "Address@Email",}))
    class Meta:
        model = GuestEmail
        fields = [
            'email',
        ]

    def __init__(self, request, *args, **kwargs):
        self.request = request
        super(GuestForm, self).__init__(*args, **kwargs)

    def save(self, commit=True):
        # Save the provided password in hashed format
        obj = super(GuestForm, self).save(commit=False)
        if commit:
            obj.save()
            request = self.request
            request.session['guest_email_id'] = obj.id
        return obj
    
class LoginForm(forms.Form):
    username = forms.CharField(label='Username', widget=forms.TextInput(attrs={"class": "form-control", "placeholder": "Username",}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={"class": "form-control", "placeholder": "Password",}))

    def __init__(self, request, *args, **kwargs):
        self.request = request
        super(LoginForm, self).__init__(*args, **kwargs)

    def clean(self):
        request = self.request
        data = self.cleaned_data
        username = data.get("username")
        password = data.get("password")
        qs = User.objects.filter(username=username)
        if qs.exists():
            # User email is registered check if active/email activation
            not_active = qs.filter(is_active=False)
            if not_active.exists():
                # Not active, check email activation
                link = reverse('account:resend-activation')
                reconfirm_msg="""Go to <a href='{resend_link}'>
                resend confirmation email</a>.
                """.format(resend_link=link)
                email= User.objects.get(username=username).email
                confirm_email = EmailActivation.objects.filter(email=email)
                is_confirmable = confirm_email.confirmable().exists()
                if is_confirmable:
                    msg1 = "Please check your email to confirm your account or " + reconfirm_msg.lower()
                    raise forms.ValidationError(mark_safe(msg1))
                email_confirm_exists = EmailActivation.objects.email_exists(email).exists()
                if email_confirm_exists:
                    msg2 = "Email not confirmed. " + reconfirm_msg
                    raise forms.ValidationError(mark_safe(msg2))
                if not is_confirmable and not email_confirm_exists:
                    raise forms.ValidationError("This user is inactive.")
        user = authenticate(request, username=username, password=password)
        if user is None:
            raise forms.ValidationError("Invalid credentials")
        # if user.is_authenticated and user.premium:
        #     User.objects.premium_check(user)
        login(request, user)
        self.user=user
        return data


class RegisterForm(forms.ModelForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={'class':'form-control', 'Placeholder':'Password 1',}))
    password2 = forms.CharField(label='Confirm Password', widget=forms.PasswordInput(attrs={'class':'form-control', 'Placeholder':'Password 2',}))

    class Meta:
        model = User
        fields = ('first_name', 'last_name','email', 'role', 'username', 'about', 'website', 'linkedin',)  # 'full-name',)

    def __init__(self, *args, **kwargs):
        super(RegisterForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].widget.attrs.update( {'class':'form-control', 'Placeholder':'First Name',})
        self.fields['last_name'].widget.attrs.update( {'class':'form-control', 'Placeholder':'Last Name',})
        self.fields['email'].widget.attrs.update( {'class':'form-control', 'Placeholder':'Email',})
        self.fields['role'].widget.attrs.update( {'class':'form-control', 'Placeholder':'Role',})
        self.fields['username'].widget.attrs.update( {'class':'form-control', 'Placeholder':'Username',})
        self.fields['about'].widget.attrs.update( {'class':'form-control', 'Placeholder':'About',})
        self.fields['website'].widget.attrs.update( {'class':'form-control', 'Placeholder':'Website',})
        self.fields['linkedin'].widget.attrs.update( {'class':'form-control', 'Placeholder':'LinkedIn',})

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(RegisterForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        user.is_active = False # Send a confirmation email 
        if commit:
            user.save()
        return user

class TeamRegisterForm(forms.ModelForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""
    # password1 = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={'class':'form-control', 'Placeholder':'Password 1',}))
    # password2 = forms.CharField(label='Confirm Password', widget=forms.PasswordInput(attrs={'class':'form-control', 'Placeholder':'Password 2',}))

    class Meta:
        model = Team
        fields = ('name', 'role', 'email', 'image','display','disp_order', 'summary', 'about', 'website', 'linkedin')  # 'full-name',)

    def __init__(self, *args, **kwargs):
        super(TeamRegisterForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs.update( {'class':'form-control', 'Placeholder':'Full Name',})
        self.fields['role'].widget.attrs.update( {'class':'form-control', 'Placeholder':'Role',})
        self.fields['summary'].widget.attrs.update( {'class':'form-control', 'Placeholder':'Summary',})
        self.fields['about'].widget.attrs.update( {'class':'form-control', 'Placeholder':'About',})
        self.fields['image'].widget.attrs.update( {'class':'form-control',})
        self.fields['email'].widget.attrs.update( {'class':'form-control', 'Placeholder':'Email',})
        self.fields['website'].widget.attrs.update( {'class':'form-control', 'Placeholder':'Website',})
        self.fields['linkedin'].widget.attrs.update( {'class':'form-control', 'Placeholder':'LinkedIn',})
        self.fields['display'].widget.attrs.update( {'class':'form-control',})
        self.fields['disp_order'].widget.attrs.update( {'class':'form-control',})

class TeamDetailChangeForm(forms.ModelForm):
    name = forms.CharField(label='Name', required=False, widget=forms.TextInput(attrs={"class": 'form-control'}))
    role = forms.CharField(label='Role', required=False, widget=forms.TextInput(attrs={"class": 'form-control'}))
    email = forms.CharField(label='Email', required=False, widget=forms.TextInput(attrs={"class": 'form-control'}))
    # image = forms.CharField(label='Image', required=False, widget=forms.FileInput(attrs={"class": 'form-control'}))
    disp_order = forms.IntegerField(label='Display Order', required=False, widget=forms.NumberInput(attrs={"class": 'form-control'}))
    summary = forms.CharField(label='Summary', required=False, widget=forms.Textarea(attrs={"class": 'form-control'}))
    about = forms.CharField(label='About', required=False, widget=forms.Textarea(attrs={"class": 'form-control'}))
    website = forms.CharField(label='Website', required=False, widget=forms.TextInput(attrs={"class": 'form-control'}))
    linkedin = forms.CharField(label='LinkedIn', required=False, widget=forms.TextInput(attrs={"class": 'form-control'}))

    class Meta:
        model = Team
        fields = ['name', 'role', 'email', 'image','display','disp_order', 'summary', 'about', 'website', 'linkedin']