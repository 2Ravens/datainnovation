from django.contrib.auth import authenticate, login, get_user_model
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.utils.http import is_safe_url
from django.utils.safestring import mark_safe
from django.views.generic import CreateView, FormView, DetailView, View, UpdateView
from django.views.generic.edit import FormMixin

from .forms import LoginForm, RegisterForm, GuestForm, ReactivateEmailForm, UserDetailChangeForm, TeamRegisterForm, TeamDetailChangeForm
from .models import GuestEmail, EmailActivation
from .signals import user_logged_in

from home.models import Team

from DataInnovation.mixins import NextUrlMixin, RequestFormAttachedMixin

# @login_required #/automatically goes to accounts/login/?next=/some/path/
# def account_home_view(request):
# 	return render(request, 'accounts/home.html', {})

class AccountHomeView(LoginRequiredMixin, DetailView):
	template_name = 'accounts/home.html'
	def get_object(self):
		return self.request.user

class AccountEmailActivationView(FormMixin, View):
	success_url = '/login/'
	form_class = ReactivateEmailForm

	def get(self, request, key=None, *args, **kwargs):
		self.key = key
		if key is not None:
			qs = EmailActivation.objects.filter(key__iexact=key)
			confirm_qs = qs.confirmable()
			if confirm_qs.count() == 1:
				obj = confirm_qs.first()
				obj.activate()
				messages.success(request, "Your email has been confirmed. Please login.")
				return redirect('login')
			else:
				activated_qs = qs.filter(activated=True)
				if activated_qs.exists():
					reset_link=reverse('password_reset')
					msg = """Your email has already been confirmed
					Do you need to <a href="{link}">reset your password</a>?
					""".format(link=reset_link)
					messages.success(request, mark_safe(msg))
					return redirect('login')
		context = {'form':self.get_form(), 'key': key}
		return render(request, 'registration/activation-error.html', context)

	def post(self, request, *args, **kwargs):
		form = self.get_form()
		if form.is_valid():
			return self.form_valid(form)
		else:
			return self.form_invalid(form)

	def form_valid(self, form):
		msg = """Activation link sent, please check your email."""
		request = self.request
		messages.success(request, msg)
		email = form.cleaned_data.get('email')
		obj = EmailActivation.objects.email_exists(email).first()
		user = obj.user
		new_activation = EmailActivation.objects.create(user=user, email=email)
		new_activation.send_activation()
		return super(AccountEmailActivationView, self).form_valid(form)
	
	def form_invalid(self, form):
		context = {'form':form, 'key': self.key}
		return render(self.request, 'registration/activation-error.html', context)

class GuestRegisterView(NextUrlMixin, RequestFormAttachedMixin, CreateView):
	form_class = GuestForm
	default_next = '/register/'

	def get_success_url(self):
		return self.get_next_url()

	def form_invalid(self, form):
		return redirect(self.default_next)

class LoginView(NextUrlMixin, RequestFormAttachedMixin, FormView):
	form_class = LoginForm
	success_url = '/'
	template_name = 'accounts/login.html'
	default_next = '/'

	def form_valid(self, form):
		next_path = self.get_next_url()
		return redirect(next_path)

class RegisterView(CreateView):
	form_class = RegisterForm
	template_name = 'accounts/register.html'
	success_url = '/login/'

	@method_decorator(login_required) # Make sure user is logged in   
	def dispatch(self, *args, **kwargs):
		return super().dispatch(*args, **kwargs)

class TeamRegisterView(LoginRequiredMixin, CreateView):
	form_class = TeamRegisterForm
	template_name = 'accounts/member-register.html'
	success_url = '/account/'

	@method_decorator(login_required) # Make sure user is logged in   
	def dispatch(self, *args, **kwargs):
		return super().dispatch(*args, **kwargs)

class TeamDetailUpdateView(LoginRequiredMixin, UpdateView):
	form_class = TeamDetailChangeForm
	template_name = 'accounts/team-detail-update-form.html'
	success_url = '/account/'

	def get_object(self):
		pk = self.kwargs.get(self.pk_url_kwarg)
		print(pk)
		return Team.objects.get(pk=pk)

	def get_context_data(self, *args, **kwargs):
		context = super(TeamDetailUpdateView, self).get_context_data(*args, **kwargs)
		# print(context)
		context['title'] = 'Change your account details'
		return context

		# {
		# 	'object': <SimpleLazyObject: <User: admin>>, 
		# 	'user': <SimpleLazyObject: <User: admin>>, 
		# 	'form': <TeamDetailChangeForm bound=False, valid=Unknown, fields=(name)>, 
		# 	'view': <accounts.views.TeamDetailUpdateView object at 0x0000017EBF7E32B0>
		# }
	def get_success_url(self):
		return reverse('account:home')


class UserDetailUpdateView(LoginRequiredMixin, UpdateView):
	form_class = UserDetailChangeForm
	template_name = 'accounts/detail-update-form.html'
	success_url = '/account/'

	def get_object(self):
		return self.request.user

	def get_context_data(self, *args, **kwargs):
		context = super(UserDetailUpdateView, self).get_context_data(*args, **kwargs)
		context['title'] = 'Change your account details'
		return context
	
	def get_success_url(self):
		return reverse('account:home')

###

# def login_page(request):
# 	form = LoginForm(request.POST or None)
# 	context = {
# 		"form": form,
# 	}
	# next_ = request.GET.get('next')
	# next_post = request.POST.get('next')
	# redirect_path = next_ or next_post or None
# 	if form.is_valid():
# 		username = form.cleaned_data.get("username")
# 		password = form.cleaned_data.get("password")
# 		user = authenticate(request, username=username, password=password)

# 		if user is not None:
# 			login(request, user)
# 			try:
# 				del request.session['guest_email_id']
# 			except:
# 				pass
# 			if is_safe_url(redirect_path, request.get_host()):
# 				return redirect(redirect_path)
# 			else:
# 				return redirect("/")
# 		else:
# 			# Return an 'invalid login' error message.
# 			print("Error")

	# return render(request, "accounts/login.html", context)

# User = get_user_model()
# def register_page(request):
# 	form = RegisterForm(request.POST or None)
# 	context = {
# 		"form": form,
# 	}
# 	if form.is_valid():
# 		form.save()
# 		# print(form.cleaned_data)
# 		# username = form.cleaned_data.get("username")
# 		# email = form.cleaned_data.get("email")
# 		# password = form.cleaned_data.get("password")
# 		# new_user = User.objects.create_user(username, email, password)
# 		# print(new_user)

# 	return render(request, "accounts/register.html", context)
