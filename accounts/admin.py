from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

# Register your models here.

from .forms import UserAdminCreationForm, UserAdminChangeForm
from .models import GuestEmail, EmailActivation

User = get_user_model()


class UserAdminCustom(BaseUserAdmin):
    # The forms to add and change user instances
    # form = UserAdminChangeForm
    # add_form = UserAdminCreationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('first_name', 'last_name', 'username', 'email', 'display', 'admin')
    list_filter = ('admin', 'staff', 'active', 'display')
    fieldsets = (
        (None, {'fields': ('first_name', 'last_name', 'username', 'role', 'about', 'image',)}),
        ('Address', {'fields': ('email', 'website', 'linkedin')}), # Put in details for address
        ('Permissions', {'fields': ('admin', 'staff', 'is_active', 'display', 'disp_order')}),
    )
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2')}
         ),
    )
    search_fields = ('email','username',)
    ordering = ('email',)
    filter_horizontal = ()

admin.site.register(User, UserAdminCustom)

# Remove Group Model from admin. We're not using it.
admin.site.unregister(Group)

class EmailActivationAdmin(admin.ModelAdmin):
    search_fields = ['email']
    class Meta:
        model = EmailActivation

admin.site.register(EmailActivation, EmailActivationAdmin)

class GuestEmailAdmin(admin.ModelAdmin):
    search_fields = ['email']

    class Meta:
        model = GuestEmail

admin.site.register(GuestEmail, GuestEmailAdmin)
