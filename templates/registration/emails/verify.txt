Hello,

Please verify your account for {{ email }} by clicking the link below:

{{ path }}

Once activated, you can login!

Thanks,

2 Ravens