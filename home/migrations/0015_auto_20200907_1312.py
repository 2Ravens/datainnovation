# Generated by Django 3.0.8 on 2020-09-07 12:12

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0014_awards'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Awards',
            new_name='Award',
        ),
    ]
