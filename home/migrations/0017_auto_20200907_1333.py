# Generated by Django 3.0.8 on 2020-09-07 12:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0016_auto_20200907_1330'),
    ]

    operations = [
        migrations.AlterField(
            model_name='team',
            name='email',
            field=models.EmailField(blank=True, max_length=255, null=True),
        ),
    ]
