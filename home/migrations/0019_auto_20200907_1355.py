# Generated by Django 3.0.8 on 2020-09-07 12:55

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0018_remove_team_username'),
    ]

    operations = [
        migrations.RenameField(
            model_name='team',
            old_name='first_name',
            new_name='name',
        ),
    ]
