# Generated by Django 3.0.8 on 2020-09-10 14:44

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0028_auto_20200910_1126'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='award',
            options={'ordering': ['disp_order']},
        ),
        migrations.AlterModelOptions(
            name='news',
            options={'ordering': ['-published']},
        ),
        migrations.AlterModelOptions(
            name='partner',
            options={'ordering': ['disp_order']},
        ),
        migrations.AlterModelOptions(
            name='solution',
            options={'ordering': ['disp_order']},
        ),
        migrations.AlterModelOptions(
            name='team',
            options={'ordering': ['disp_order']},
        ),
    ]
