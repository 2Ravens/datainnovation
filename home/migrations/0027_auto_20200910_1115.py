# Generated by Django 3.0.8 on 2020-09-10 10:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0026_auto_20200909_1157'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='award',
            name='duration',
        ),
        migrations.RemoveField(
            model_name='award',
            name='planned_start',
        ),
        migrations.RemoveField(
            model_name='partner',
            name='duration',
        ),
        migrations.RemoveField(
            model_name='partner',
            name='planned_start',
        ),
        migrations.AddField(
            model_name='solution',
            name='featured',
            field=models.BooleanField(default=False),
        ),
    ]
