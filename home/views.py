from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.conf import settings
from django.core.mail import send_mail
from django.contrib.auth import get_user_model
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.utils.decorators import method_decorator
from django.views import generic

from accounts.models import User

from .forms import *
from .models import *
# Create your views here.

user = User


def home(request):
	"""
	This function is for the home view/the front page.
	This is where Home2 is called if user 'is_authenticated'.
	"""
	context = {
		'solution': Solution.objects.filter(featured=True),
		'front': True,
		'edit_solution': 'about:edit_solution',
		'delete_solution': 'about:delete_solution',
		'link_solution': 'about:solution_detail',
		'news': News.objects.all()[:3],
		'edit': 'edit_news',
		'delete': 'delete_news',
	}
	if request.user.is_authenticated:
		return render(request, "home2.html", context)
	else:
		return render(request, "home.html", context)


def about(request):
	"""
	This calls the About page
	"""
	context = {
		'title': 'About Us',
		'message': 'About Page',
	}
	return render(request, "about/about.html", context)

def vision(request):
	"""
	This function is for the Vision page, where you can write what the company's aim is.
	"""
	context = {
		'title': 'Our Vision',
		'message': 'About Page: Vision',
	}
	return render(request, "about/vision.html", context)

def history(request):
	"""
	This references the History page where the company's history can be compiled.
	"""
	context = {
		'title': 'Our History',
		'message': 'About Page: History',
	}
	return render(request, "about/history.html", context)

def contact(request):
	"""
	This is the contact page and also where the contact form deals with teh email request to send a message to equiries@DataInovation.AI.
	"""
	contact_form = ContactForm(request.POST or None)
	context = {
		"title":"Contact Page",
		"content": "Contact us and lets start making decisions!",
		"form":contact_form,
	}
	if contact_form.is_valid():
		form = contact_form.cleaned_data
		um = UserMessage()
		um.fullname = form['fullname']
		um.email = form['email']
		um.subject = form['subject']
		um.message = form['content']
		message = f"From: {um.fullname},\n\nEmail: {um.email},\n\nSubject: {um.subject},\n\nMessage:\n {um.message}"
		admin_email = settings.DEFAULT_FROM_EMAIL
		send_mail(
			um.subject,
			message,
			admin_email,
			[admin_email],            
		)
		um.save()
		if request.is_ajax():
			return JsonResponse({"message": "Thank you for your message"})

	if contact_form.errors:
		errors = contact_form.errors.as_json()
		if request.is_ajax():
			return HttpResponse(errors, status=400, content_type='application/json')
	return render(request, "contact.html", context)

"""
Team Views
"""
class TeamRegisterView(LoginRequiredMixin, generic.CreateView):
	"""
	This model is to create a Team member object using the oject from forms.py
	"""
	form_class = TeamRegisterForm
	template_name = 'home/register.html'
	success_url = '/about/team/'

	def get_context_data(self, **kwargs):
		self.context = super().get_context_data(**kwargs)
		self.context['title'] = 'Team member register'
		self.context['message'] = 'Register new members to the team to be displayed on the Our Team page.'
		return self.context
	

class TeamView(generic.ListView):
	"""
	This is where the general Teams view is, where all members are displayed on cards
	"""
	model=Team     
	template_name = "about/about-cards.html"

	def get_context_data(self, **kwargs):
		self.context = super().get_context_data(**kwargs)
		self.context['title'] = 'Our Team'
		self.context['create'] = 'about:create_team'
		self.context['element'] = 'Team Member'
		self.context['team'] = True
		self.context['link'] = 'about:team_detail'
		self.context['edit'] = 'about:edit_team'
		self.context['delete'] = 'about:delete_team'
		return self.context

class TeamDetailView(generic.DetailView):
	"""
	This object is to view specific team members in detail
	"""
	model = Team     
	template_name = "about/about-detail.html"

	def get_object(self):
		return get_object_or_404(Team.objects.filter(display=True), pk=self.kwargs.get(self.pk_url_kwarg))

	def get_context_data(self, *args, **kwargs):
		self.context = super(TeamDetailView, self).get_context_data(*args, **kwargs)
		self.context['team'] = True
		return self.context

class TeamDetailUpdateView(LoginRequiredMixin, generic.UpdateView):
	"""
	This is where the team member details are updated
	"""
	form_class = TeamDetailChangeForm
	template_name = 'home/detail-update-form.html'
	success_url = '/about/team/'

	def get_object(self):
		pk = self.kwargs.get(self.pk_url_kwarg)
		return Team.objects.get(pk=pk)

	def get_context_data(self, *args, **kwargs):
		context = super(TeamDetailUpdateView, self).get_context_data(*args, **kwargs)
		self.context['title'] = 'Edit Team Member'
		context['cancel'] = 'about:team_url'
		return context

class TeamDelete(LoginRequiredMixin, generic.DeleteView):
	"""
	This is where the member delete function is called
	"""
	model = Team
	template_name = 'base/delete_view.html'
	success_url = '/about/team/'

"""
Solution Views
"""
 
class SolutionRegisterView(LoginRequiredMixin, generic.CreateView):
	form_class = SolutionRegisterForm
	template_name = 'home/register.html'
	success_url = '/about/solution/'

	def get_context_data(self, **kwargs):
		self.context = super().get_context_data(**kwargs)
		self.context['title'] = 'Solution Creation'
		self.context['message'] = 'Register new solution to be displayed on the Solution page.'
		return self.context

class SolutionView(generic.ListView):
	model = Solution     
	template_name = "about/about-cards.html"

	def get_context_data(self, **kwargs):
		self.context = super().get_context_data(**kwargs)
		self.context['title'] = 'Our Solutions'
		self.context['create'] = 'about:create_solution'
		self.context['element'] = 'Solution'
		self.context['link'] = 'about:solution_detail'
		self.context['edit'] = 'about:edit_solution'
		self.context['delete'] = 'about:delete_solution'
		return self.context

class SolutionDetailView(generic.DetailView):
	model = Solution     
	template_name = "about/about-detail.html"

	def get_object(self):
		return get_object_or_404(Solution.objects.filter(display=True), pk=self.kwargs.get(self.pk_url_kwarg))

class SolutionDetailUpdateView(LoginRequiredMixin, generic.UpdateView):
	form_class = SolutionDetailChangeForm
	template_name = 'home/detail-update-form.html'
	success_url = '/about/solution/'

	def get_object(self):
		pk = self.kwargs.get(self.pk_url_kwarg)
		return Solution.objects.get(pk=pk)

	def get_context_data(self, *args, **kwargs):
		context = super(SolutionDetailUpdateView, self).get_context_data(*args, **kwargs)
		self.context['title'] = 'Edit a Solution'
		context['cancel'] = 'about:solution_url'
		return context

class SolutionDelete(LoginRequiredMixin, generic.DeleteView):
	model = Solution
	template_name = 'base/delete_view.html'
	success_url = '/about/solution/'

"""
Partner Views
"""
class PartnerRegisterView(LoginRequiredMixin, generic.CreateView):
	form_class = PartnerRegisterForm
	template_name = 'home/register.html'
	success_url = '/about/partner/'

	def get_context_data(self, **kwargs):
		self.context = super().get_context_data(**kwargs)
		self.context['title'] = 'Register Partner'
		self.context['message'] = 'Register new partner to be displayed on the Partner page.'
		return self.context

class PartnerView(generic.ListView):
	model = Partner     
	template_name = "about/about-cards.html"

	def get_context_data(self, **kwargs):
		self.context = super().get_context_data(**kwargs)
		self.context['title'] = 'Our Partners'
		self.context['create'] = 'about:create_partner'
		self.context['element'] = 'Partner'
		self.context['link'] = 'about:partner_detail'
		self.context['edit'] = 'about:edit_partner'
		self.context['delete'] = 'about:delete_partner'
		return self.context

class PartnerDetailView(generic.DetailView):
	model = Partner     
	template_name = "about/about-detail.html"

	def get_object(self):
		return get_object_or_404(Partner.objects.filter(display=True), pk=self.kwargs.get(self.pk_url_kwarg))

class PartnerDetailUpdateView(LoginRequiredMixin, generic.UpdateView):
	form_class = PartnerDetailChangeForm
	template_name = 'home/detail-update-form.html'
	success_url = '/about/partner/'

	def get_object(self):
		pk = self.kwargs.get(self.pk_url_kwarg)
		return Partner.objects.get(pk=pk)

	def get_context_data(self, *args, **kwargs):
		context = super(PartnerDetailUpdateView, self).get_context_data(*args, **kwargs)
		self.context['title'] = 'Update Partner'
		context['cancel'] = 'about:partner_url'
		return context

class PartnerDelete(LoginRequiredMixin, generic.DeleteView):
	model = Partner
	template_name = 'base/delete_view.html'
	success_url = '/about/partner/'

"""
Award Views
"""

class AwardRegisterView(LoginRequiredMixin, generic.CreateView):
	form_class = AwardRegisterForm
	template_name = 'home/register.html'
	success_url = '/about/award/'

	def get_context_data(self, **kwargs):
		self.context = super().get_context_data(**kwargs)
		self.context['title'] = 'Award Creation'
		self.context['message'] = 'Register new award to be displayed on the Award page.'
		return self.context

class AwardView(generic.ListView):
	model = Award     
	template_name = "about/about-cards.html"

	def get_context_data(self, **kwargs):
		self.context = super().get_context_data(**kwargs)
		self.context['title'] = 'Our Awards'
		self.context['create'] = 'about:create_award'
		self.context['element'] = 'Award'
		self.context['link'] = 'about:award_detail'
		self.context['edit'] = 'about:edit_award'
		self.context['delete'] = 'about:delete_award'
		return self.context

class AwardDetailView(generic.DetailView):
	model = Award
	template_name = "about/about-detail.html"

	def get_object(self):
		return get_object_or_404(Award.objects.filter(display=True), pk=self.kwargs.get(self.pk_url_kwarg))

class AwardDetailUpdateView(LoginRequiredMixin, generic.UpdateView):
	form_class = AwardDetailChangeForm
	template_name = 'home/detail-update-form.html'
	success_url = '/about/award/'

	def get_object(self):
		pk = self.kwargs.get(self.pk_url_kwarg)
		print(pk)
		return Award.objects.get(pk=pk)

	def get_context_data(self, *args, **kwargs):
		context = super(AwardDetailUpdateView, self).get_context_data(*args, **kwargs)
		self.context['title'] = 'Update Award'
		context['cancel'] = 'about:award_url'
		return context

class AwardDelete(LoginRequiredMixin, generic.DeleteView):
	model = Award
	template_name = 'base/delete_view.html'
	success_url = '/about/award/'



"""
News Pages
"""

class NewsRegisterView(LoginRequiredMixin, generic.CreateView):
	form_class = NewsRegisterForm
	template_name = 'home/register.html'
	success_url = '/news/'

	def get_context_data(self, **kwargs):
		self.context = super().get_context_data(**kwargs)
		self.context['title'] = 'Write News'
		self.context['message'] = 'Write about new events to be displayed on the News page.'
		return self.context

class NewsView(generic.ListView):
	model = News
	template_name = "news/news-cards.html"

	def get_context_data(self, **kwargs):
		self.context = super().get_context_data(**kwargs)
		self.context['title'] = 'Our Latest News'
		self.context['message'] = "Keep an eye out for any updates!"
		self.context['create'] = 'create_news'
		self.context['element'] = 'News'
		self.context['link'] = 'news_detail'
		self.context['edit'] = 'edit_news'
		self.context['delete'] = 'delete_news'
		return self.context

class NewsDetailView(generic.DetailView):
	model = News
	template_name = "news/news-detail.html"

	def get_object(self):
		return get_object_or_404(News.objects.filter(display=True), pk=self.kwargs.get(self.pk_url_kwarg))

	def get_context_data(self, **kwargs):
		self.context = super().get_context_data(**kwargs)
		return self.context

class NewsDetailUpdateView(LoginRequiredMixin, generic.UpdateView):
	form_class = NewsDetailChangeForm
	template_name = 'home/detail-update-form.html'
	success_url = '/news/'

	def get_object(self):
		return News.objects.get(pk=self.kwargs.get(self.pk_url_kwarg))

	def get_context_data(self, *args, **kwargs):
		context = super(NewsDetailUpdateView, self).get_context_data(*args, **kwargs)
		self.context['title'] = 'Update News'
		context['cancel'] = 'news_url'
		return context

class NewsDelete(LoginRequiredMixin, generic.DeleteView):
	model = News
	template_name = 'base/delete_view.html'
	success_url = '/news/'
