from django.contrib import admin
from .models import UserMessage, Solution, Partner, Award, Team, News
# Register your models here.

admin.site.register(UserMessage)

admin.site.register(Solution)

admin.site.register(Partner)

admin.site.register(Award)

admin.site.register(Team)

admin.site.register(News)