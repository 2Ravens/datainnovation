from django.conf.urls import url
from django.urls import path

from . import views

app_name = 'about'

urlpatterns = [
    
    url(r'^$', views.about, name='about_url'),
    url(r'^vision/', views.vision, name='vision_url'),
    url(r'^history/', views.history, name='history_url'),

    # path('<slug:element>/', views.AboutView.as_view()), # Overview of content
    # path('<slug:element>/<int:pk>/', views.AboutDetailView.as_view()), # Detail View of content
    
    url(r'^team/create/$', views.TeamRegisterView.as_view(), name='create_team'),
    path('team/', views.TeamView.as_view(), name='team_url'), # Detail View of content
    path('team/<int:pk>/', views.TeamDetailView.as_view(), name='team_detail'), # Detail View of content
    path('team/<int:pk>/edit', views.TeamDetailUpdateView.as_view(), name='edit_team'),
    path('team/<int:pk>/delete/', views.TeamDelete.as_view(), name='delete_team'),

    url(r'^solution/create$', views.SolutionRegisterView.as_view(), name='create_solution'),
    path('solution/', views.SolutionView.as_view(), name='solution_url'), # Detail View of content
    path('solution/<int:pk>/', views.SolutionDetailView.as_view(), name='solution_detail'), # Detail View of content
    path('solution/<int:pk>/edit', views.SolutionDetailUpdateView.as_view(), name='edit_solution'),
    path('solution/<int:pk>/delete/', views.SolutionDelete.as_view(), name='delete_solution'),

    url(r'^partner/create$', views.PartnerRegisterView.as_view(), name='create_partner'),
    path('partner/', views.PartnerView.as_view(), name='partner_url'), # Detail View of content
    path('partner/<int:pk>/', views.PartnerDetailView.as_view(), name='partner_detail'), # Detail View of content
    path('partner/<int:pk>/edit', views.PartnerDetailUpdateView.as_view(), name='edit_partner'),
    path('partner/<int:pk>/delete/', views.PartnerDelete.as_view(), name='delete_partner'),

    url(r'^award/create$', views.AwardRegisterView.as_view(), name='create_award'),
    path('award/', views.AwardView.as_view(), name='award_url'), # Detail View of content
    path('award/<int:pk>/', views.AwardDetailView.as_view(), name='award_detail'), # Detail View of content
    path('award/<int:pk>/edit', views.AwardDetailUpdateView.as_view(), name='edit_award'),
    path('award/<int:pk>/delete/', views.AwardDelete.as_view(), name='delete_award'),

]


# account/email/confirm/asdasfd/ -> activaiton view