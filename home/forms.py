from django import forms
from django.contrib.auth.models import User

from .models import Team, Solution, Partner, Award, News

class ContactForm(forms.Form):
	fullname = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control", "placeholder":"Your full name", "id": "form_full_name"}))
	email = forms.EmailField(widget=forms.EmailInput(attrs={'class':'form-control', "placeholder":"Address@Email",}))
	subject = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control", "placeholder":"Subject", "id": "subject"}))
	content = forms.CharField(widget=forms.Textarea(attrs={'class':'form-control', "placeholder":"Your message",}))

class TeamRegisterForm(forms.ModelForm):
	"""A form for creating new team members. Includes all the required fields."""

	class Meta:
		model = Team
		fields = ('name', 'role', 'email', 'image', 'summary', 'about', 'display','disp_order', 'website', 'linkedin')  # 'full-name',)

	def __init__(self, *args, **kwargs):
		super(TeamRegisterForm, self).__init__(*args, **kwargs)
		self.fields['name'].widget.attrs.update( {'class':'form-control', 'Placeholder':'Full Name',})
		self.fields['role'].widget.attrs.update( {'class':'form-control', 'Placeholder':'Role',})
		self.fields['email'].widget.attrs.update( {'class':'form-control', 'Placeholder':'Email',})
		self.fields['image'].widget.attrs.update( {'class':'form-control',})
		self.fields['summary'].widget.attrs.update( {'class':'form-control', 'Placeholder':'Summary',})
		self.fields['about'].widget.attrs.update( {'class':'form-control', 'Placeholder':'About',})
		# self.fields['display'].widget.attrs.update( {'class':'form-control',})
		self.fields['disp_order'].widget.attrs.update( {'class':'form-control',})
		self.fields['website'].widget.attrs.update( {'class':'form-control', 'Placeholder':'Website',})
		self.fields['linkedin'].widget.attrs.update( {'class':'form-control', 'Placeholder':'LinkedIn',})

class TeamDetailChangeForm(forms.ModelForm):
	"""Form for updating the Team Members"""
	name = forms.CharField(label='Name', required=False, widget=forms.TextInput(attrs={"class": 'form-control'}))
	role = forms.CharField(label='Role', required=False, widget=forms.TextInput(attrs={"class": 'form-control'}))
	email = forms.CharField(label='Email', required=False, widget=forms.TextInput(attrs={"class": 'form-control'}))
	image = forms.ImageField(label='Image', required=False, widget=forms.FileInput(attrs={"class": 'form-control'}))
	summary = forms.CharField(label='Summary', required=False, widget=forms.Textarea(attrs={"class": 'form-control'}))
	about = forms.CharField(label='About', required=False, widget=forms.Textarea(attrs={"class": 'form-control'}))
	disp_order = forms.IntegerField(label='Display Order', required=False, widget=forms.NumberInput(attrs={"class": 'form-control'}))
	website = forms.CharField(label='Website', required=False, widget=forms.TextInput(attrs={"class": 'form-control'}))
	linkedin = forms.CharField(label='LinkedIn', required=False, widget=forms.TextInput(attrs={"class": 'form-control'}))

	class Meta:
		model = Team
		fields = ['name', 'role', 'email', 'image', 'summary', 'about', 'display','disp_order', 'website', 'linkedin']


class SolutionRegisterForm(forms.ModelForm):
	"""A form for creating new team members. Includes all the required fields."""

	class Meta:
		model = Solution
		fields = ('name', 'title', 'project_lead', 'image', 'summary', 'about', 'featured', 'display','disp_order', 'website', 'linkedin')  # 'full-name',)

	def __init__(self, *args, **kwargs):
		super(SolutionRegisterForm, self).__init__(*args, **kwargs)
		self.fields['name'].widget.attrs.update( {'class':'form-control', 'Placeholder':'Name',})
		self.fields['title'].widget.attrs.update( {'class':'form-control', 'Placeholder':'Title',})
		self.fields['project_lead'].widget.attrs.update( {'class':'form-control', 'Placeholder':'Project Lead',})
		self.fields['image'].widget.attrs.update( {'class':'form-control',})
		self.fields['summary'].widget.attrs.update( {'class':'form-control', 'Placeholder':'Summary',})
		self.fields['about'].widget.attrs.update( {'class':'form-control', 'Placeholder':'About',})
		self.fields['featured'].widget.attrs.update( {'class':'form-control',})
		# self.fields['display'].widget.attrs.update( {'class':'form-control',})
		self.fields['disp_order'].widget.attrs.update( {'class':'form-control',})
		self.fields['website'].widget.attrs.update( {'class':'form-control', 'Placeholder':'Website',})
		self.fields['linkedin'].widget.attrs.update( {'class':'form-control', 'Placeholder':'LinkedIn',})

        

class SolutionDetailChangeForm(forms.ModelForm):
	"""Form for updating the Team Members"""
	name = forms.CharField(label='Name', required=False, widget=forms.TextInput(attrs={"class": 'form-control'}))
	title = forms.CharField(label='Summary', required=False, widget=forms.Textarea(attrs={"class": 'form-control'}))
	project_lead = forms.ModelChoiceField(label='Project Leader', queryset=Team.objects.all(), empty_label="Select a Project Lead", required=False, widget=forms.Select(attrs={"class": 'form-control'})) # https://docs.djangoproject.com/en/3.1/ref/forms/fields/#choicefield
	# project_lead = forms.CharField(label='Project Lead', required=False, widget=forms.Select(attrs={"class": 'form-control'}))
	image = forms.ImageField(label='Image', required=False, widget=forms.FileInput(attrs={"class": 'form-control'}))
	summary = forms.CharField(label='Summary', required=False, widget=forms.Textarea(attrs={"class": 'form-control'}))
	about = forms.CharField(label='About', required=False, widget=forms.Textarea(attrs={"class": 'form-control'}))
	disp_order = forms.IntegerField(label='Display Order', required=False, widget=forms.NumberInput(attrs={"class": 'form-control'}))
	website = forms.CharField(label='Website', required=False, widget=forms.TextInput(attrs={"class": 'form-control'}))
	linkedin = forms.CharField(label='LinkedIn', required=False, widget=forms.TextInput(attrs={"class": 'form-control'}))

	class Meta:
		model = Solution
		fields = ['name', 'title', 'project_lead', 'image', 'summary', 'about', 'featured', 'display','disp_order', 'website', 'linkedin']


class PartnerRegisterForm(forms.ModelForm):
	"""A form for creating new team members. Includes all the required fields."""

	class Meta:
		model = Partner
		fields = ('name', 'solution', 'image', 'summary', 'about', 'display','disp_order', 'website', 'linkedin')  # 'full-name',)

	def __init__(self, *args, **kwargs):
		super(PartnerRegisterForm, self).__init__(*args, **kwargs)
		self.fields['name'].widget.attrs.update( {'class':'form-control', 'Placeholder':'Name',})
		self.fields['solution'].widget.attrs.update( {'class':'form-control', 'Placeholder':'Solution',})
		self.fields['image'].widget.attrs.update( {'class':'form-control',})
		self.fields['summary'].widget.attrs.update( {'class':'form-control', 'Placeholder':'Summary',})
		self.fields['about'].widget.attrs.update( {'class':'form-control', 'Placeholder':'About',})
		# self.fields['display'].widget.attrs.update( {'class':'form-control',})
		self.fields['disp_order'].widget.attrs.update( {'class':'form-control',})
		self.fields['website'].widget.attrs.update( {'class':'form-control', 'Placeholder':'Website',})
		self.fields['linkedin'].widget.attrs.update( {'class':'form-control', 'Placeholder':'LinkedIn',})
        

class PartnerDetailChangeForm(forms.ModelForm):
	"""Form for updating the Team Members"""
	name = forms.CharField(label='Name', required=False, widget=forms.TextInput(attrs={"class": 'form-control'}))
	solution = forms.ModelChoiceField(label='Solution', queryset=Solution.objects.all(), empty_label="Select a Solution", required=False, widget=forms.Select(attrs={"class": 'form-control'})) # https://docs.djangoproject.com/en/3.1/ref/forms/fields/#choicefield
	image = forms.ImageField(label='Image', required=False, widget=forms.FileInput(attrs={"class": 'form-control'}))
	summary = forms.CharField(label='Summary', required=False, widget=forms.Textarea(attrs={"class": 'form-control'}))
	about = forms.CharField(label='About', required=False, widget=forms.Textarea(attrs={"class": 'form-control'}))
	disp_order = forms.IntegerField(label='Display Order', required=False, widget=forms.NumberInput(attrs={"class": 'form-control'}))
	website = forms.CharField(label='Website', required=False, widget=forms.TextInput(attrs={"class": 'form-control'}))
	linkedin = forms.CharField(label='LinkedIn', required=False, widget=forms.TextInput(attrs={"class": 'form-control'}))

	class Meta:
		model = Partner
		fields = ['name', 'solution', 'image', 'summary', 'about', 'display','disp_order', 'website', 'linkedin'] 

class AwardRegisterForm(forms.ModelForm):
	"""A form for creating new team members. Includes all the required fields."""

	class Meta:
		model = Award
		fields = ('name', 'solution', 'image', 'summary', 'about', 'display','disp_order', 'website', 'linkedin')  # 'full-name',)

	def __init__(self, *args, **kwargs):
		super(AwardRegisterForm, self).__init__(*args, **kwargs)
		self.fields['name'].widget.attrs.update( {'class':'form-control', 'Placeholder':'Name',})
		self.fields['solution'].widget.attrs.update( {'class':'form-control', 'Placeholder':'Solution',})
		self.fields['image'].widget.attrs.update( {'class':'form-control',})
		self.fields['summary'].widget.attrs.update( {'class':'form-control', 'Placeholder':'Summary',})
		self.fields['about'].widget.attrs.update( {'class':'form-control', 'Placeholder':'About',})
		# self.fields['display'].widget.attrs.update( {'class':'form-control',})
		self.fields['disp_order'].widget.attrs.update( {'class':'form-control',})
		self.fields['website'].widget.attrs.update( {'class':'form-control', 'Placeholder':'Website',})
		self.fields['linkedin'].widget.attrs.update( {'class':'form-control', 'Placeholder':'LinkedIn',})
        

class AwardDetailChangeForm(forms.ModelForm):
	"""Form for updating the Team Members"""
	name = forms.CharField(label='Name', required=False, widget=forms.TextInput(attrs={"class": 'form-control'}))
	solution = forms.ModelChoiceField(label='Solution', queryset=Solution.objects.all(), empty_label="Select a Solution", required=False, widget=forms.Select(attrs={"class": 'form-control'})) # https://docs.djangoproject.com/en/3.1/ref/forms/fields/#choicefield
	# solution = forms.CharField(label='Solution', required=False, widget=forms.Select(attrs={"class": 'form-control'}))
	image = forms.ImageField(label='Image', required=False, widget=forms.FileInput(attrs={"class": 'form-control'}))
	summary = forms.CharField(label='Summary', required=False, widget=forms.Textarea(attrs={"class": 'form-control'}))
	about = forms.CharField(label='About', required=False, widget=forms.Textarea(attrs={"class": 'form-control'}))
	disp_order = forms.IntegerField(label='Display Order', required=False, widget=forms.NumberInput(attrs={"class": 'form-control'}))
	website = forms.CharField(label='Website', required=False, widget=forms.TextInput(attrs={"class": 'form-control'}))
	linkedin = forms.CharField(label='LinkedIn', required=False, widget=forms.TextInput(attrs={"class": 'form-control'}))

	class Meta:
		model = Award
		fields = ['name', 'solution', 'image', 'summary', 'about', 'display','disp_order', 'website', 'linkedin'] 

class NewsRegisterForm(forms.ModelForm):
	"""A form for creating new team members. Includes all the required fields."""

	class Meta:
		model = News
		fields = ('title', 'solution', 'image', 'summary', 'story', 'display')  # 'full-name',)

	def __init__(self, *args, **kwargs):
		super(NewsRegisterForm, self).__init__(*args, **kwargs)
		self.fields['title'].widget.attrs.update( {'class':'form-control', 'Placeholder':'Title',})
		self.fields['solution'].widget.attrs.update( {'class':'form-control', 'Placeholder':'Solution',})
		self.fields['image'].widget.attrs.update( {'class':'form-control',})
		self.fields['summary'].widget.attrs.update( {'class':'form-control', 'Placeholder':'Summary',})
		self.fields['story'].widget.attrs.update( {'class':'form-control', 'Placeholder':'Story',})
		# self.fields['display'].widget.attrs.update( {'class':'form-control',})        

class NewsDetailChangeForm(forms.ModelForm):
	"""Form for updating the Team Members"""
	title = forms.CharField(label='Title', required=False, widget=forms.TextInput(attrs={"class": 'form-control'}))
	solution = forms.ModelChoiceField(label='Solution', queryset=Solution.objects.all(), empty_label="Select a Solution", required=False, widget=forms.Select(attrs={"class": 'form-control'})) # https://docs.djangoproject.com/en/3.1/ref/forms/fields/#choicefield
	image = forms.ImageField(label='Image', required=False, widget=forms.FileInput(attrs={"class": 'form-control'}))
	summary = forms.CharField(label='Summary', required=False, widget=forms.Textarea(attrs={"class": 'form-control'}))
	story = forms.CharField(label='Story', required=False, widget=forms.Textarea(attrs={"class": 'form-control'}))

	class Meta:
		model = News
		fields = ['title', 'solution', 'image', 'summary', 'story', 'display'] 

