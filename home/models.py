from django.contrib.auth import get_user_model

from django.db import models

from accounts.models import User

class UserMessage(models.Model):
    """
    This saves a copy of the messages sent to equiries@DataInnovation.AI inbox
    """ 
    fullname = models.CharField(max_length=100, null=True, blank=True)
    subject =  models.CharField(max_length=200, null=True, blank=True)
    email = models.EmailField(max_length=255)
    message = models.TextField(max_length=2000, null=True, blank=True)

    def __str__(self):
        return self.email

class Team(models.Model):
    """
    This is the Team model
    email - Save an email, if null it will not show email icon the website
    name - Full name of the member to be referenced as the title of the member
    role - What position are they in the company?
    summary - Breif blurb to appear on the card, string only.
    about - More indepth blurb that will appear in a detail view, can use String or HTML
    linkedin - To add a link to the member's linked in profile
    website - Add personal website
    image - What image to display on the card and the head image for a detail view
    display - Show card publicly or hide so that only admin may view it.
    disp_order - What order to display members in. If null will come first, then member 1, then 2...
    timestamp - Maybe add "Member since..."
    """
    email     = models.EmailField(max_length=255, null=True, blank=True)
    name      = models.CharField(max_length=255, blank=True, null=True)
    role      = models.CharField(max_length=255, blank=True, null=True)
    summary   = models.TextField(max_length=2000, blank=True, null=True)
    about     = models.TextField(max_length=2000, blank=True, null=True)
    linkedin  = models.URLField(max_length=255, blank=True, null=True)
    website   = models.URLField(max_length=255, blank=True, null=True)
    image     = models.ImageField(upload_to='static/img/',null=True, blank=True)
    display   = models.BooleanField(default=False) 
    disp_order= models.IntegerField(null=True, blank=True)
    timestamp = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['disp_order']

    def __str__(self):
        return self.name
        
class Solution(models.Model):
    name            = models.CharField(max_length=200)
    title           = models.TextField(max_length=1000, null=True, blank=True)
    summary         = models.TextField(max_length=500, null=True, blank=True)
    about           = models.TextField(max_length=50000, null=True, blank=True)
    project_lead    = models.ForeignKey(Team, null=True, blank=True, on_delete=models.SET_NULL)
    linkedin        = models.URLField(max_length=255, blank=True, null=True)
    website         = models.URLField(max_length=255, blank=True, null=True)
    image           = models.ImageField(upload_to='static/img/',null=True, blank=True)
    display         = models.BooleanField(default=False) 
    disp_order      = models.IntegerField(null=True, blank=True)
    duration        = models.CharField(max_length=200, null=True, blank=True)
    planned_start   = models.DateTimeField(null=True, blank=True)
    featured        = models.BooleanField(default=False) 

    class Meta:
        ordering = ['disp_order']

    def __str__(self):
        return self.name

class Partner(models.Model):
    name            = models.CharField(max_length=200)
    summary         = models.TextField(max_length=500, null=True, blank=True)
    about           = models.TextField(max_length=10000, null=True, blank=True)
    solution        = models.ForeignKey(Solution, null=True, blank=True, on_delete=models.SET_NULL)
    linkedin        = models.URLField(max_length=255, blank=True, null=True)
    website         = models.URLField(max_length=255, blank=True, null=True)
    image           = models.ImageField(upload_to='static/img/',null=True, blank=True)
    display         = models.BooleanField(default=False) 
    disp_order      = models.IntegerField(null=True, blank=True)

    class Meta:
        ordering = ['disp_order']

    def __str__(self):
        return self.name

class Award(models.Model):
    name            = models.CharField(max_length=200)
    summary         = models.TextField(max_length=500, null=True, blank=True)
    about           = models.TextField(max_length=10000, null=True, blank=True)
    solution        = models.ForeignKey(Solution, null=True, blank=True, on_delete=models.SET_NULL)
    linkedin        = models.URLField(max_length=255, blank=True, null=True)
    website         = models.URLField(max_length=255, blank=True, null=True)
    image           = models.ImageField(upload_to='static/img/',null=True, blank=True)
    display         = models.BooleanField(default=False) 
    disp_order      = models.IntegerField(null=True, blank=True)

    class Meta:
        ordering = ['disp_order']

    def __str__(self):
        return self.name

class News(models.Model):
    title           = models.CharField(max_length=200)
    summary         = models.TextField(max_length=500, null=True, blank=True)
    story           = models.TextField(max_length=10000, null=True, blank=True)
    solution        = models.ForeignKey(Solution, null=True, blank=True, on_delete=models.SET_NULL)
    image           = models.ImageField(upload_to='static/img/',null=True, blank=True)
    display         = models.BooleanField(default=False) 
    published       = models.DateTimeField(auto_now_add=True, null=True, blank=True)

    class Meta:
        ordering = ['-published']

    def __str__(self):
        return self.title